package com.epam.rd.java.basic.task7.db;

public class Constants {
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_TEAM_ID = "team_id";

    public static final String SELECT_ALL_USERS = "SELECT * FROM users";
    public static final String SELECT_USER_WITH_LOGIN = "SELECT * FROM users WHERE " + COLUMN_LOGIN + " = ?";
    public static final String INSERT_USER = "INSERT INTO users(" + COLUMN_LOGIN + ") VALUES(?)";
    public static final String DELETE_USER = "DELETE FROM users WHERE " + COLUMN_ID + " = ?";

    public static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
    public static final String SELECT_TEAMS_WITH_ID = "SELECT * FROM teams WHERE " + COLUMN_ID + " = ?";
    public static final String SELECT_TEAM_WITH_NAME = "SELECT * FROM teams WHERE " + COLUMN_NAME + " = ?";
    public static final String INSERT_TEAM = "INSERT INTO teams(" + COLUMN_NAME + ") VALUES(?)";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE " + COLUMN_NAME + " = ?";
    public static final String UPDATE_TEAM = "UPDATE teams SET " + COLUMN_NAME + " = ? WHERE " + COLUMN_ID + " = ?";

    public static final String SELECT_ALL_USERS_TEAMS = "SELECT " + COLUMN_TEAM_ID + " FROM users_teams WHERE "
            + COLUMN_USER_ID + " = ?";
    public static final String INSERT_USERS_TEAMS = String.format("INSERT INTO users_teams(%s, %s) VALUES(?,?)"
            , COLUMN_USER_ID, COLUMN_TEAM_ID);


    private Constants() {
    }
}
