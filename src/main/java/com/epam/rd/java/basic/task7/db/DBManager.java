package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;
    private String url;

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
        try (FileInputStream fileInputStream = new FileInputStream("app.properties")) {
            Properties properties = new Properties();
            properties.load(fileInputStream);
            url = properties.getProperty("connection.url").replace("connection.url=", "");
        } catch (IOException e) {
            throw new IllegalStateException("Cannot get connection url from the property file", e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> list = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url)) {
            PreparedStatement pstmt = conn.prepareStatement(Constants.SELECT_ALL_USERS);
            ResultSet resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                final User user = User.createUser(resultSet.getString(Constants.COLUMN_LOGIN));
                user.setId(resultSet.getInt(Constants.COLUMN_ID));
                list.add(user);
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }

        return list;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = DriverManager.getConnection(url);
             PreparedStatement statement = connection.prepareStatement(Constants.INSERT_USER,
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new DBException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                } else {
                    throw new DBException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DBException("Connection by url = " + url + " failed", e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.DELETE_USER)) {
            for (User user : users) {
                final int id = user.getId();
                pstmt.setInt(1, id);
                if (pstmt.executeUpdate() == 0) {
                    throw new DBException("Deleting of user with id =" + id + " failed");
                }
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.SELECT_USER_WITH_LOGIN)) {
            pstmt.setString(1, login);
            final ResultSet userRS = pstmt.executeQuery();
            if (userRS.next()) {
                user.setId(userRS.getInt(Constants.COLUMN_ID));
                user.setLogin(userRS.getString(Constants.COLUMN_LOGIN));
            } else {
                throw new DBException("There are any user with login = " + login);
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.SELECT_TEAM_WITH_NAME)) {
            pstmt.setString(1, name);
            final ResultSet teamRS = pstmt.executeQuery();

            if (teamRS.next()) {
                team.setId(teamRS.getInt(Constants.COLUMN_ID));
                team.setName(teamRS.getString(Constants.COLUMN_NAME));
            } else {
                throw new DBException("There are any user with login = " + name);
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> list = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.SELECT_ALL_TEAMS)) {
            ResultSet resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                final Team team = Team.createTeam(resultSet.getString(Constants.COLUMN_NAME));
                team.setId(resultSet.getInt(Constants.COLUMN_ID));
                list.add(team);
            }
        } catch (SQLException e) {
            throw new DBException(String.format("Cannot connect by url = %s", url), e);
        }

        return list;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.INSERT_TEAM,
                     Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, team.getName());

            int affectedRows = pstmt.executeUpdate();

            if (affectedRows == 0) {
                throw new DBException("Creating team failed, no rows affected.");
            }

            try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    team.setId(generatedKeys.getInt(1));
                } else {
                    throw new DBException("Creating team failed, no ID obtained.");
                }
            }

        } catch (SQLException e) {
            throw new DBException("Connection by url = " + url + " failed", e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try (Connection conn = DriverManager.getConnection(url)) {
            conn.setAutoCommit(false);

            try(PreparedStatement pstmt = conn.prepareStatement(Constants.INSERT_USERS_TEAMS)){
                for (Team team : teams) {
                    pstmt.setInt(1, user.getId());
                    pstmt.setInt(2, team.getId());
                    pstmt.executeUpdate();
                }
                conn.commit();
            }catch(SQLException er){
                conn.rollback();
                throw new DBException("Part of transaction was denied. Rollback...", er);
            }

        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.SELECT_ALL_USERS_TEAMS)) {
            pstmt.setInt(1, user.getId());

            final ResultSet teamsIdRS = pstmt.executeQuery();
            while (teamsIdRS.next()) {
                try (PreparedStatement ps = conn.prepareStatement(Constants.SELECT_TEAMS_WITH_ID)) {
                    final int teamsId = teamsIdRS.getInt(Constants.COLUMN_TEAM_ID);
                    ps.setInt(1, teamsId);

                    final ResultSet teamsRS = ps.executeQuery();

                    if (teamsRS.next()) {
                        final Team team = Team.createTeam(teamsRS.getString(Constants.COLUMN_NAME));
                        team.setId(teamsRS.getInt(Constants.COLUMN_ID));
                        list.add(team);
                    } else {
                        throw new DBException("There are any team with id = " + teamsId);
                    }
                }catch (SQLException e ){
                    throw new DBException("Inner " , e);
                }
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.DELETE_TEAM)) {
            final String name = team.getName();
            pstmt.setString(1, name);
            if (pstmt.executeUpdate() == 0) {
                throw new DBException("Deleting of team with name =" + name + " failed");
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(url);
             PreparedStatement pstmt = conn.prepareStatement(Constants.UPDATE_TEAM)) {
            final int id = team.getId();

            pstmt.setInt(2, id);
            pstmt.setString(1, team.getName());

            if (pstmt.executeUpdate() == 0) {
                throw new DBException("Updating of team with id =" + id + " failed");
            }
        } catch (SQLException er) {
            throw new DBException("Connection by url = " + url + " failed", er);
        }
        return true;
    }

}
